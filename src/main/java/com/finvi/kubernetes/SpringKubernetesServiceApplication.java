package com.finvi.kubernetes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringKubernetesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringKubernetesServiceApplication.class, args);
	}

}
